﻿/*Napravite igru križić-kružić (iks-oks) korištenjem znanja stečenih na ovoj
laboratorijskoj vježbi. Omogućiti pokretanje igre, unos imena dvaju igrača, ispis
koji igrač je trenutno na potezu, igranje igre s iscrtavanjem križića i kružića na
odgovarajućim mjestima te ispis dijaloga s porukom o pobjedi, odnosno
neriješenom rezultatu kao i praćenje ukupnog rezultata.*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Krizic_Kruzic
{
    public partial class Form1 : Form
    {
        bool f = true;
        int brojac;
        static string igrac1, igrac2;

        public static void PostaviImena(string a, string b)
        {
            igrac1 = a;
            igrac2 = b;
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void novaIgraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f = true;
            brojac = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

                catch { }
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (f)
            {
                b.Text = "X";
                label.Text = igrac2 + " je na potezu!";

            }
            else
            {
                b.Text = "O";
                label.Text = igrac1 + " je na potezu!";

            }
            f = !f;
            b.Enabled = false;
            brojac++;
            provjeri_pobjednika();
        }

       

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void provjeri_pobjednika()
        {
            bool ima_pobjednik = false;
            //vodoravna provjera
            if ((BTN1.Text == BTN2.Text) && (BTN2.Text == BTN3.Text) && (!BTN1.Enabled))
            { ima_pobjednik = true; }
            else if ((BTN4.Text == BTN5.Text) && (BTN5.Text == BTN6.Text) && (!BTN4.Enabled))
            { ima_pobjednik = true; }
            else if ((BTN7.Text == BTN8.Text) && (BTN8.Text == BTN9.Text) && (!BTN7.Enabled))
            { ima_pobjednik = true; }
            //okomita provjera
            else if ((BTN1.Text == BTN4.Text) && (BTN4.Text == BTN7.Text) && (!BTN1.Enabled))
            { ima_pobjednik = true; }
            else if ((BTN2.Text == BTN5.Text) && (BTN5.Text == BTN8.Text) && (!BTN2.Enabled))
            { ima_pobjednik = true; }
            else if ((BTN3.Text == BTN6.Text) && (BTN6.Text == BTN9.Text) && (!BTN3.Enabled))
            { ima_pobjednik = true; }
            //dijagonalna provjera
            else if ((BTN1.Text == BTN5.Text) && (BTN5.Text == BTN9.Text) && (!BTN1.Enabled))
            { ima_pobjednik = true; }
            else if ((BTN3.Text == BTN5.Text) && (BTN5.Text == BTN7.Text) && (!BTN3.Enabled))
            { ima_pobjednik = true; }


            if (ima_pobjednik)
            {
                disable_buttons();
                String pobjednik = "";
                if (f)
                {
                    pobjednik = igrac2;
                    pobjedao.Text = (Int32.Parse(pobjedao.Text) + 1).ToString();

                }
                else
                {
                    pobjednik = igrac1;
                    pobjedax.Text = (Int32.Parse(pobjedax.Text) + 1).ToString();
                }
                MessageBox.Show(pobjednik + " je pobjedio!", "Rezultat");
            }//end if
            else
            {
                if (brojac == 9)
                {
                    izjednaceno.Text = (Int32.Parse(izjednaceno.Text) + 1).ToString();
                    MessageBox.Show("Izjednaceno!", "Rezultat");
                }
            }

        }

        private void disable_buttons()
        {

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
                catch { }
            }


        }

        private void button_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (f)
                {
                    b.Text = "X";
                }
                else b.Text = "O";
            }

        }

        private void poništiRezultateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pobjedax.Text = "0";
            pobjedao.Text = "0";
            izjednaceno.Text = "0";

            f = true;
            brojac = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

                catch { }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
            label.Text = igrac1 + " je na potezu!";
            label1.Text = igrac1;
            label3.Text = igrac2;
            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.BackColor = Color.Green;
                }

                catch { }
            }
        }

        private void button_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }

        }

    }
}
