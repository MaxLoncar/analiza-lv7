﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Krizic_Kruzic
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        

        private void igrac2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "\r")
                BTN.PerformClick();
        }

        private void BTN_Click(object sender, EventArgs e)
        {
            Form1.PostaviImena(tb1.Text, tb2.Text);
            this.Close();
        }
    }
}
