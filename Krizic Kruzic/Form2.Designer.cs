﻿namespace Krizic_Kruzic
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb1 = new System.Windows.Forms.TextBox();
            this.tb2 = new System.Windows.Forms.TextBox();
            this.igrac1 = new System.Windows.Forms.Label();
            this.igrac2 = new System.Windows.Forms.Label();
            this.BTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb1
            // 
            this.tb1.Location = new System.Drawing.Point(131, 44);
            this.tb1.Name = "tb1";
            this.tb1.Size = new System.Drawing.Size(100, 20);
            this.tb1.TabIndex = 0;
            // 
            // tb2
            // 
            this.tb2.Location = new System.Drawing.Point(131, 94);
            this.tb2.Name = "tb2";
            this.tb2.Size = new System.Drawing.Size(100, 20);
            this.tb2.TabIndex = 1;
            // 
            // igrac1
            // 
            this.igrac1.AutoSize = true;
            this.igrac1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.igrac1.Location = new System.Drawing.Point(31, 44);
            this.igrac1.Name = "igrac1";
            this.igrac1.Size = new System.Drawing.Size(62, 20);
            this.igrac1.TabIndex = 2;
            this.igrac1.Text = "Igrač 1:";
            // 
            // igrac2
            // 
            this.igrac2.AutoSize = true;
            this.igrac2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.igrac2.Location = new System.Drawing.Point(31, 94);
            this.igrac2.Name = "igrac2";
            this.igrac2.Size = new System.Drawing.Size(62, 20);
            this.igrac2.TabIndex = 3;
            this.igrac2.Text = "Igrač 2:";
            // 
            // BTN
            // 
            this.BTN.Location = new System.Drawing.Point(174, 133);
            this.BTN.Name = "BTN";
            this.BTN.Size = new System.Drawing.Size(57, 31);
            this.BTN.TabIndex = 4;
            this.BTN.Text = "Unesi";
            this.BTN.UseVisualStyleBackColor = true;
            this.BTN.Click += new System.EventHandler(this.BTN_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(258, 182);
            this.Controls.Add(this.BTN);
            this.Controls.Add(this.igrac2);
            this.Controls.Add(this.igrac1);
            this.Controls.Add(this.tb2);
            this.Controls.Add(this.tb1);
            this.Name = "Form2";
            this.Text = "Unos igrača";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb1;
        private System.Windows.Forms.TextBox tb2;
        private System.Windows.Forms.Label igrac1;
        private System.Windows.Forms.Label igrac2;
        private System.Windows.Forms.Button BTN;
    }
}