﻿namespace Krizic_Kruzic
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaIgraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.poništiRezultateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izlazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BTN1 = new System.Windows.Forms.Button();
            this.BTN2 = new System.Windows.Forms.Button();
            this.BTN3 = new System.Windows.Forms.Button();
            this.BTN4 = new System.Windows.Forms.Button();
            this.BTN5 = new System.Windows.Forms.Button();
            this.BTN6 = new System.Windows.Forms.Button();
            this.BTN7 = new System.Windows.Forms.Button();
            this.BTN8 = new System.Windows.Forms.Button();
            this.BTN9 = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pobjedax = new System.Windows.Forms.Label();
            this.izjednaceno = new System.Windows.Forms.Label();
            this.pobjedao = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(259, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaIgraToolStripMenuItem,
            this.poništiRezultateToolStripMenuItem,
            this.izlazToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // novaIgraToolStripMenuItem
            // 
            this.novaIgraToolStripMenuItem.Name = "novaIgraToolStripMenuItem";
            this.novaIgraToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.novaIgraToolStripMenuItem.Text = "Nova igra";
            this.novaIgraToolStripMenuItem.Click += new System.EventHandler(this.novaIgraToolStripMenuItem_Click);
            // 
            // poništiRezultateToolStripMenuItem
            // 
            this.poništiRezultateToolStripMenuItem.Name = "poništiRezultateToolStripMenuItem";
            this.poništiRezultateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.poništiRezultateToolStripMenuItem.Text = "Poništi rezultate";
            this.poništiRezultateToolStripMenuItem.Click += new System.EventHandler(this.poništiRezultateToolStripMenuItem_Click);
            // 
            // izlazToolStripMenuItem
            // 
            this.izlazToolStripMenuItem.Name = "izlazToolStripMenuItem";
            this.izlazToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.izlazToolStripMenuItem.Text = "Izlaz";
            this.izlazToolStripMenuItem.Click += new System.EventHandler(this.izlazToolStripMenuItem_Click);
            // 
            // BTN1
            // 
            this.BTN1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN1.Location = new System.Drawing.Point(12, 52);
            this.BTN1.Name = "BTN1";
            this.BTN1.Size = new System.Drawing.Size(75, 75);
            this.BTN1.TabIndex = 2;
            this.BTN1.UseVisualStyleBackColor = true;
            this.BTN1.Click += new System.EventHandler(this.button_Click);
            // 
            // BTN2
            // 
            this.BTN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN2.Location = new System.Drawing.Point(93, 52);
            this.BTN2.Name = "BTN2";
            this.BTN2.Size = new System.Drawing.Size(75, 75);
            this.BTN2.TabIndex = 3;
            this.BTN2.UseVisualStyleBackColor = true;
            this.BTN2.Click += new System.EventHandler(this.button_Click);
            // 
            // BTN3
            // 
            this.BTN3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN3.Location = new System.Drawing.Point(174, 52);
            this.BTN3.Name = "BTN3";
            this.BTN3.Size = new System.Drawing.Size(75, 75);
            this.BTN3.TabIndex = 4;
            this.BTN3.UseVisualStyleBackColor = true;
            this.BTN3.Click += new System.EventHandler(this.button_Click);
            // 
            // BTN4
            // 
            this.BTN4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN4.Location = new System.Drawing.Point(12, 133);
            this.BTN4.Name = "BTN4";
            this.BTN4.Size = new System.Drawing.Size(75, 75);
            this.BTN4.TabIndex = 5;
            this.BTN4.UseVisualStyleBackColor = true;
            this.BTN4.Click += new System.EventHandler(this.button_Click);
            // 
            // BTN5
            // 
            this.BTN5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN5.Location = new System.Drawing.Point(93, 133);
            this.BTN5.Name = "BTN5";
            this.BTN5.Size = new System.Drawing.Size(75, 75);
            this.BTN5.TabIndex = 6;
            this.BTN5.UseVisualStyleBackColor = true;
            this.BTN5.Click += new System.EventHandler(this.button_Click);
            // 
            // BTN6
            // 
            this.BTN6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN6.Location = new System.Drawing.Point(174, 133);
            this.BTN6.Name = "BTN6";
            this.BTN6.Size = new System.Drawing.Size(75, 75);
            this.BTN6.TabIndex = 7;
            this.BTN6.UseVisualStyleBackColor = true;
            this.BTN6.Click += new System.EventHandler(this.button_Click);
            // 
            // BTN7
            // 
            this.BTN7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN7.Location = new System.Drawing.Point(12, 214);
            this.BTN7.Name = "BTN7";
            this.BTN7.Size = new System.Drawing.Size(75, 75);
            this.BTN7.TabIndex = 8;
            this.BTN7.UseVisualStyleBackColor = true;
            this.BTN7.Click += new System.EventHandler(this.button_Click);
            // 
            // BTN8
            // 
            this.BTN8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN8.Location = new System.Drawing.Point(93, 214);
            this.BTN8.Name = "BTN8";
            this.BTN8.Size = new System.Drawing.Size(75, 75);
            this.BTN8.TabIndex = 9;
            this.BTN8.UseVisualStyleBackColor = true;
            this.BTN8.Click += new System.EventHandler(this.button_Click);
            // 
            // BTN9
            // 
            this.BTN9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BTN9.Location = new System.Drawing.Point(174, 214);
            this.BTN9.Name = "BTN9";
            this.BTN9.Size = new System.Drawing.Size(75, 75);
            this.BTN9.TabIndex = 10;
            this.BTN9.UseVisualStyleBackColor = true;
            this.BTN9.Click += new System.EventHandler(this.button_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(12, 36);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(0, 13);
            this.label.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 292);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Broj pobjeda(X):";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(100, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Izjednačeno:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(174, 292);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Broj pobjeda(O):";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pobjedax
            // 
            this.pobjedax.AutoSize = true;
            this.pobjedax.Location = new System.Drawing.Point(42, 305);
            this.pobjedax.Name = "pobjedax";
            this.pobjedax.Size = new System.Drawing.Size(13, 13);
            this.pobjedax.TabIndex = 15;
            this.pobjedax.Text = "0";
            this.pobjedax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // izjednaceno
            // 
            this.izjednaceno.AutoSize = true;
            this.izjednaceno.Location = new System.Drawing.Point(124, 305);
            this.izjednaceno.Name = "izjednaceno";
            this.izjednaceno.Size = new System.Drawing.Size(13, 13);
            this.izjednaceno.TabIndex = 16;
            this.izjednaceno.Text = "0";
            this.izjednaceno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pobjedao
            // 
            this.pobjedao.AutoSize = true;
            this.pobjedao.Location = new System.Drawing.Point(203, 305);
            this.pobjedao.Name = "pobjedao";
            this.pobjedao.Size = new System.Drawing.Size(13, 13);
            this.pobjedao.TabIndex = 17;
            this.pobjedao.Text = "0";
            this.pobjedao.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 325);
            this.Controls.Add(this.pobjedao);
            this.Controls.Add(this.izjednaceno);
            this.Controls.Add(this.pobjedax);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label);
            this.Controls.Add(this.BTN9);
            this.Controls.Add(this.BTN8);
            this.Controls.Add(this.BTN7);
            this.Controls.Add(this.BTN6);
            this.Controls.Add(this.BTN5);
            this.Controls.Add(this.BTN4);
            this.Controls.Add(this.BTN3);
            this.Controls.Add(this.BTN2);
            this.Controls.Add(this.BTN1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Krizic Kruzic";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaIgraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem poništiRezultateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izlazToolStripMenuItem;
        private System.Windows.Forms.Button BTN1;
        private System.Windows.Forms.Button BTN2;
        private System.Windows.Forms.Button BTN3;
        private System.Windows.Forms.Button BTN4;
        private System.Windows.Forms.Button BTN5;
        private System.Windows.Forms.Button BTN6;
        private System.Windows.Forms.Button BTN7;
        private System.Windows.Forms.Button BTN8;
        private System.Windows.Forms.Button BTN9;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label pobjedax;
        private System.Windows.Forms.Label izjednaceno;
        private System.Windows.Forms.Label pobjedao;
    }
}

